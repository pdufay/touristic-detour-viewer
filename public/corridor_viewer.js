CorridorViewer = function (map) {
    var map_markers = [];

    this.display_route = function (departure, destination, set_status) {
        map.getSource('route_data').setData({
            'type': 'FeatureCollection',
            'features': []
        });
        map.getSource('closest_lines_data').setData({
            'type': 'FeatureCollection',
            'features': []
        });
        map_markers.forEach((marker, index) => { marker.remove() });

        let url = 'http://40.68.38.44/calculateRoute?motorization=fuel&type=recommended&';
        url += 'locations=' + departure.lat + ',' + departure.lng + ';' + destination.lat + ',' + destination.lng;
        url += '&target_env=pmr_local&alternatives=false&overview=full&steps=true&mentions=all&language=en&geometries=polyline&';
        url += 'corParams=[{%22limit%22:1000,%22max_distance%22:20000,%22poi_type%22:%22scenic_route%22}]';
        console.log(url);

        set_status("Please wait...");
        $.getJSON(url, function (data) {
            if (data.code != 'Ok') {
                set_status("A problem occured: " + data.code);
                return;
            }

            var route_geom = get_geometry_line_with_offset('polyline5', data.routes[0].geometry, 0);

            map.getSource('route_data').setData({
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route_geom
                }
            });

            var pois = data['poi_descriptions'];
            var closest_lines = [];
            var turf_line = turf.lineString(route_geom);
            pois.forEach((poi, index) => {
                //map_markers.push(new maplibregl.Marker().setLngLat(poi.poi_location).addTo(map));
                var turf_point = turf.point(poi.poi_location);
                var closest = turf.nearestPointOnLine(turf_line, turf_point);
                closest_lines.push({
                    'type': 'Feature',
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': [poi.poi_location, closest.geometry.coordinates]
                    }
                });
            });

            map.getSource('closest_lines_data').setData({
                'type': 'FeatureCollection',
                'features': closest_lines
            });

            set_status("POI count: " + pois.length + ' (<a href="' + url + ' target ="_blank">source</a>)');
        });
    }
}

get_geometry_line_with_offset = function (geometry_format, route_geometry, offset) {
    if (geometry_format == 'geojson') {
        lonlat_list = []
        for (var i = 0; i < route_geometry.coordinates.length; i++) {
            lat_c = route_geometry.coordinates[i][1]
            lon_c = route_geometry.coordinates[i][0]
            ratio_lon = Math.cos(Math.PI * lat_c / 180)
            lonlat_list.push([lon_c + ratio_lon * offset, lat_c + offset])
        }
        return lonlat_list
    }
    latlon_coordinates = decode_polyline(route_geometry, geometry_format == 'polyline6' ? 6 : 5)
    lonlat_list = []
    for (var i = 0; i < latlon_coordinates.length; i++) {
        lat_c = latlon_coordinates[i][0]
        lon_c = latlon_coordinates[i][1]
        ratio_lon = Math.cos(Math.PI * lat_c / 180)
        lonlat_list.push([lon_c + ratio_lon * offset, lat_c + offset])
    }
    return lonlat_list;
}

decode_polyline = function (str, precision) {
    var index = 0,
        lat = 0,
        lng = 0,
        coordinates = [],
        shift = 0,
        result = 0,
        byte = null,
        latitude_change,
        longitude_change,
        factor = Math.pow(10, Number.isInteger(precision) ? precision : 5);

    // Coordinates have variable length when encoded, so just keep
    // track of whether we've hit the end of the string. In each
    // loop iteration, a single coordinate is decoded.
    while (index < str.length) {

        // Reset shift, result, and byte
        byte = null;
        shift = 0;
        result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        latitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        shift = result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        longitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        lat += latitude_change;
        lng += longitude_change;

        coordinates.push([lat / factor, lng / factor]);
    }

    return coordinates;
};